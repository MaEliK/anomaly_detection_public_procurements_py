# -*- coding: utf-8 -*-
import json
import hashlib
from datetime import datetime
import psycopg2


title_row = []
title_rows = []
rows = []
row = []
# indir = '/home/maeli/PycharmProjects/change_version_1_1_ocds/pruebas_datos/merged.json'
ettiquetes_switch = {
    "abastecimiento_simultaneo": 1,
    "adreferendum": 2,
    "agricultura_familiar": 3,
    "bs_estrategicos": 4,
    "contrato_abierto": 5,
    "fonacide": 6,
    "impugnado": 7,
    "plurianual": 8,
    "produccion_nacional": 9,
    "seguridad_nacional": 10,
    "subasta": 11,
    "urgencia_impostergable": 12,
}
status_switcher = {
    "COMPROMETIDO": 1,
    "ANULADO": 2,
    "EN PROCESO": 3,
    "PREVISIONADO": 4,
}
format_switcher = {
    "html": 1,
    "application/pdf": 2,
    "application/zip": 3,
    "text/plain": 4,
}
language_switcher = {
    "en": 1,
    "es": 2,
}
documentType_switcher = {
    "x_budgetAvailabilityCertificate": 1,
    "Acta de Apertura": 2,
    "Adenda de Ampliación": 3,
    "Adenda de Contrato": 4,
    "awardNotice": 5,
    "contractArrangements": 6,
    "Contrato Renovación": 7,
    "Cuadro Comparativo de Ofertas": 8,
    "Dictamen de Ampliación": 9,
    "Dictamen de Rescisión": 10,
    "Dictamen Renovación": 11,
    "Documento Notificación": 12,
    "Endozo de Garantía": 13,
    "evaluationReports": 14,
    "Nota de Aclaración": 15,
    "Nota Cancelación Adjudicación": 16,
    "Nota de Contestación": 17,
    "Notificación al Oferente": 18,
    "Orden de Compra o Contrato": 19,
    "Poliza / Garantia": 20,
    "Resolución de Ampliación": 21,
    "Resolución de Cancelación Adjudicación": 22,
    "Resolución de Reajuste": 23,
    "Resolución Renovación": 24,
    "Acta de Prórroga": 25,
    "Adenda": 26,
    "Autorización de Erssan": 27,
    "Autorización del Equipo Ec. para Vehiculos": 28,
    "biddingDocuments": 29,
    "Carta de Invitación": 30,
    "Carta de invitación a los oferentes": 31,
    "CDP": 32,
    "Certificado de DGEP": 33,
    "clarifications": 34,
    "Condiciones Generales del Contrato - Bienes y Servicios": 35,
    "Condiciones Generales del Contrato - Bienes y Servicios Banco Mundial": 36,
    "Condiciones Generales del Contrato - Bienes y Servicios Complemento Nutricional": 37,
    "Condiciones Generales del Contrato - Bienes y Servicios por Subasta": 38,
    "Condiciones Generales del Contrato - Consultores BID": 39,
    "Condiciones Generales del Contrato - Obras": 40,
    "Condiciones Generales del Contrato - Obras BID": 41,
    "Condiciones Generales del Contrato - Obras por Subasta": 42,
    "Condiciones Generales del Contrato - Seguridad y Vigilancia": 43,
    "Condiciones Generales del Contrato - Seguridad y Vigilancia Subasta": 44,
    "Condiciones Generales del Contrato - Seguros": 45,
    "Condiciones Generales del Contrato - Servicios de Limpieza": 46,
    "Constancia Adreferendum": 47,
    "Constancia de previsión plurianual": 48,
    "Dictamen de Evaluación o justificación Llamado": 49,
    "Dictamen del Llamado": 50,
    "Dictamen Justificativo de Cancelación Llamado": 51,
    "Dictamen Justificativo de Declaración Desierta": 52,
    "Dictamen modificación de plantilla": 53,
    "Evaluación Impacto Ambiental": 54,
    "Factura o Contrato": 55,
    "Fotocopia de Título": 56,
    "Informe de gravámenes y dominios": 57,
    "Instrucción a los Oferentes - Bienes y Servicios": 58,
    "Instrucción a los Oferentes - Bienes y Servicios Banco Mundial": 59,
    "Instrucción a los Oferentes - Bienes y Servicios Complemento Nutricional": 60,
    "Instrucción a los Oferentes - Bienes y Servicios por Subasta": 61,
    "Instrucción a los Oferentes - Obras": 62,
    "Instrucción a los Oferentes - Obras BID": 63,
    "Instrucción a los Oferentes - Obras por Subasta": 64,
    "Instrucción a los Oferentes - Seguridad y Vigilancia": 65,
    "Instrucción a los Oferentes - Seguridad y Vigilancia Subasta": 66,
    "Instrucción a los Oferentes - Seguros": 67,
    "Instrucción a los Oferentes - Servicios de Limpieza": 68,
    "Instrucción para los Consultores": 69,
    "Justificación Contrato Abierto": 70,
    "Justificación de abastecimiento simultáneo": 71,
    "Justificación de Contratación de Carácter Internacional": 72,
    "Lista de precios": 73,
    "Llamado publicado": 74,
    "No objeción": 75,
    "Nota Comunicación": 76,
    "Nota de Comunicación Adenda/Aclaracion (opcional)": 77,
    "Nota de Contestación Llamado": 78,
    "Nota de Observacion": 79,
    "Nota de Observación de Llamado": 80,
    "Nota de Reparo": 81,
    "Nota de Respuesta Llamado": 82,
    "Nota de Suspensión": 83,
    "Otros - Audiencias": 84,
    "Países Elegibles": 85,
    "Países Elegibles - Consultores BID": 86,
    "Países Elegilbes - Obras BID": 87,
    "Permiso Municipal": 88,
    "Plano de obra o cómputos métricos": 89,
    "Precios Referenciales": 90,
    "Proforma de contrato": 91,
    "Proforma de Contrato CD": 92,
    "Publicación en periódicos": 93,
    "Resolución Cancelación Llamado": 94,
    "Resolución de Aprobación de Pliego": 95,
    "Resolución Declaración Desierta": 96,
    "Resoluciṕn del Llamado": 97,
    "Resolución Locación de Inmueble": 98,
    "Resolución Via Excepción": 99,
    "SEPA": 100,
    "technicalSpecifications": 101,
}
country_switcher = {
    "Argentina": 1,
    "Bolivia": 2,
    "Brasil": 3,
    "Canadá": 4,
    "Chile": 5,
    "China": 6,
    "Colombia": 7,
    "Eslovenia": 8,
    "España": 9,
    "FRANCIA": 10,
    "India": 11,
    "ISRAEL": 12,
    "México": 13,
    "Paraguay": 14,
    "Portugal": 15,
    "Uruguay": 16,
}
scheme_switcher = {
    "Registro Único de Contribuyente emitido por el Ministerio de Hacienda del Gobierno de Paraguay": 1,
    "PY-PGN": 2,
}
roles_switcher = {
    "supplier": 1,
    "tenderer": 2,
    "buyer": 3,
    "procuringEntity": 4,
}
tag_switcher = {
    "compiled": 1,
    "implementation": 2,
    "planning": 3,
    "tender":4,
}
award_status_switcher = {
    "active": 1,
    "unsuccessful": 2,
}
contract_status_switcher = {
    "active": 1,
    "cancelled": 2,
}
unit_name_switcher = {
    "Centimetros cubicos": 1,
    "Costo por Mil": 2,
    "Determinación": 3,
    "Gramos": 4,
    "Hectáreas": 5,
    "Kilogramos": 6,
    "Kilómetros": 7,
    "Litros": 8,
    "Mes": 9,
    "Metro lineal": 10,
    "Metros": 11,
    "Metros cuadrados": 12,
    "Metros cúbicos": 13,
    "Miligramos": 14,
    "Mililitros": 15,
    "_NO_APLICA_": 16,
    "Pulgadas": 17,
    "Ración": 18,
    "Tonelada": 19,
    "Unidad": 20,
    "Unidad Internacional": 21,
    "Unidad Medida Global": 22,
    "Año": 23,
    "Centimetros": 24,
    "Centimetros cuadrados": 25,
    "Día": 26,
    "Hora": 27,
    "Kilogramos s/ metro cuadrado": 28,
    "Milímetros": 29,
    "Milímetros cuadrados": 30,
    "Minuto": 31,
    "Por Milaje": 32,
    "Segundo": 33,
    "Yardas": 34,
}
bill_type_switcher = {
    "NOTA  DE DEBITO": 1,
    "NOTA DE DEPOSITO": 2,
    "COMPROBANTE DE VENTA": 3,
    "PLANILLA  IMPRESA": 4,
    "CONTRATO": 5,
    "FACTURA": 6,
    "BOLETOS": 7,
    "ORDEN DE PAGO": 8,
    "PLANILLA  MAGNETICA": 9,
    "NOTA  DE CREDITO": 10,
    "RECIBO": 11,
    "ORDEN DE COMPRA": 12,
    "RESOLUCION": 13,
    "NOTA DE REMISION": 14,
    "FONACIDE - FIDEICOMISO": 15,
    "COMUNICACION DE DESEMBOLSO": 16,
    "SOLICITUD TRANSFERENCIA DE RECURSOS": 17,
    "PLANILLA": 17,
    "RESOLUCION DE APROBACION DE CONTRATO (CASOS ESPECIALES)": 18,
}
retention_type_switcher = {
    "tax": 1,
    "fee": 2,
    "penalty": 3,
    "other": 4,
}
implementation_status = {
    "TRANSFERIDO": 1,
    "EMITIDO": 2,
    "INGRESADO TESORERIA": 3,
}
tender_status = {
    "active": 1,
    "cancelled": 2,
    "complete": 3,
    "unsuccessful":4,
}
submission_methods = {
    "electronicAuction": 1,
    "written": 2,
}
procurement_method_details = {
    "CONCURSO DE OFERTA": 1,
}


def getExchangeRate(moneda):
    if moneda == 'USD':
        return 5905.05
    elif moneda == 'PYG':
        return 1


def string2numeric_hash(text):
    return int(hashlib.md5(text).hexdigest()[:8], 16)


def removeDividingCharacters(text):
    new = text.replace(" ", "")
    new = new.replace("-", "")
    new = new.replace("/", "")
    return int(new)


def convertToCSV(input):
    myList = input
    if type(input) is list:
        myList = ','.join(map(str, input))
    return str(myList)


def appendNameValue(var, name):
    try:
        indice = title_row.index(name)
    except ValueError:
        indice = -1
    if type(var) is list:
        for i in var:
            if indice < 0:
                title_row.append(name)
                indice = title_row.index(name)
                row.append(i)
            else:
                row.insert(indice, i)
                if len(title_row) < len(row):
                    title_row.insert(indice, name)
            try:
                indice = title_row.index(name, indice + 1)
            except ValueError:
                indice = -1
    else:
        if indice < 0:
            title_row.append(name)
            row.append(var)
        else:
            row.insert(indice, var)
            if len(title_row) < len(row):
                title_row.insert(indice, name)


def compare_lists(a, b):
    print(len(title_rows[a]))
    print(len(title_rows[b]))
    if len(title_rows[b]) > len(title_rows[a]):
        aux = a
        a = b
        b = aux
    for i in range(len(title_rows[a])):
        if i >= len(title_rows[b]):
            print('i es mayor a title rows b')
            print(i)
            print(title_rows[a][i])
            title_rows[b].append(title_rows[a][i])
            rows[b].append(0)
            print(title_rows[a])
            print(title_rows[b])
        else:
            if title_rows[b][i] != title_rows[a][i]:
                if i > 0:
                    print(i)
                    if title_rows[b][i-1] == title_rows[b][i]:
                        print('compare with before, b')
                        print(title_rows[a][i])
                        print(title_rows[b][i])
                        title_rows[a].insert(i, title_rows[b][i])
                        rows[a].insert(i, 0)
                        print(title_rows[a])
                        print(title_rows[b])
                    else:
                        print('compare with before, a')
                        print(title_rows[a][i])
                        print(title_rows[b][i])
                        title_rows[b].insert(i, title_rows[a][i])
                        rows[b].insert(i, 0)
                        print(title_rows[a])
                        print(title_rows[b])
                else:
                    if title_rows[b][i+1] == title_rows[b][i]:
                        print('compare with after, b')
                        print(title_rows[a][i])
                        print(title_rows[b][i])
                        title_rows[a].insert(i, title_rows[b][i])
                        rows[a].insert(i, 0)
                        print(title_rows[a])
                        print(title_rows[b])
                    else:
                        print('compare with after, a')
                        print(title_rows[a][i])
                        print(title_rows[b][i])
                        title_rows[b].insert(i, title_rows[a][i])
                        rows[b].insert(i, 0)
                        print(title_rows[a])
                        print(title_rows[b])


def convertToNumeric():
    # conn = psycopg2.connect(dbname='maeli', user='tesis', password='admin_tesis')
    conn = psycopg2.connect(dbname='maeli', user='mekehler', password='Superhero')
    global row
    global title_row
    cursor = conn.cursor()
    query = "SELECT data " \
            "from contratos_v1_1_etiqueta_tenderers_hacienda limit 5"
    cursor.execute(query)
    datos = cursor.fetchall()
    cursor.close()
    del cursor
    conn.close()
    del conn
    for registro in datos:
        print(registro)
        data = json.loads(json.dumps(registro[0]))

        # records / compiledRelease / date
        if 'date' in data["records"][0]["compiledRelease"]:
            date = data["records"][0]["compiledRelease"]["date"]
            date = datetime.strptime(date, '%Y-%m-%dT%H:%M:%SZ')
        # PLANNING
        if 'planning' in data["records"][0]["compiledRelease"]:
            # ETIQUETTES
            planning_etiquetes = []
            if 'etiquettes' in data['records'][0]['compiledRelease']['planning']:
                data_ettiquetes = data["records"][0]["compiledRelease"]["planning"]["etiquettes"]
                for ettiquete in data_ettiquetes:
                    planning_etiquetes.append(ettiquetes_switch.get(ettiquete, 0))
            appendNameValue(planning_etiquetes, 'planning_etiquetes')
            # BUDGET
            planning_budget_budgetBreakdown_amount = []
            planning_budget_budgetBreakdown_commitedAmount = []
            planning_budget_budgetBreakdown_status = []
            planning_budget_budgetBreakdown_availabilityCertificateNumber = []
            planning_budget_budgetBreakdown_sourceParty = []
            # if 'budget' in data["records"][0]["compiledRelease"]["planning"]:
            #     moneda = data["records"][0]["compiledRelease"]["planning"]["budget"]["amount"]["currency"]
            #     amount = data["records"][0]["compiledRelease"]["planning"]["budget"]["amount"]["amount"]
            #     if moneda is not None and amount is not None:
            #         exchange_rate = getExchangeRate(moneda)
            #         planning_budget_amount = amount*exchange_rate
            #         appendNameValue(planning_budget_amount, 'planning_budget_amount')
            #     # BUDGET BREAKDOWN
            #     if 'budgetBreakdown' in data['records'][0]['compiledRelease']['planning']["budget"]:
            #         for i in range(len(data["records"][0]["compiledRelease"]["planning"]["budget"]["budgetBreakdown"])):
            #             # AMOUNT
            #             moneda = data["records"][0]["compiledRelease"]["planning"]["budget"]["budgetBreakdown"][i]["amount"]["currency"]
            #             amount = data["records"][0]["compiledRelease"]["planning"]["budget"]["budgetBreakdown"][i]["amount"]["amount"]
            #             exchange_rate = getExchangeRate(moneda)
            #             planning_budget_budgetBreakdown_amount.append(amount * exchange_rate)
            #             # COMMITED AMOUNT
            #             moneda = data["records"][0]["compiledRelease"]["planning"]["budget"]["budgetBreakdown"][i]["commitedAmount"]["currency"]
            #             amount = data["records"][0]["compiledRelease"]["planning"]["budget"]["budgetBreakdown"][i]["commitedAmount"]["amount"]
            #             exchange_rate = getExchangeRate(moneda)
            #             planning_budget_budgetBreakdown_commitedAmount.append(amount * exchange_rate)
            #             # STATUS
            #             status = data["records"][0]["compiledRelease"]["planning"]["budget"]["budgetBreakdown"][i]["status"]
            #             planning_budget_budgetBreakdown_status.append(status_switcher.get(status, 0))
            #             # AVAILABILITY CERTIFICATE NUMBER
            #             certificate = \
            #             data["records"][0]["compiledRelease"]["planning"]["budget"]["budgetBreakdown"][i]["availabilityCertificateNumber"]
            #             planning_budget_budgetBreakdown_availabilityCertificateNumber.append(certificate)
            #             # SOURCE PARTY ID
            #             sourceParty = \
            #                 data["records"][0]["compiledRelease"]["planning"]["budget"]["budgetBreakdown"][i][
            #                     "sourceParty"]["id"]
            #             planning_budget_budgetBreakdown_sourceParty.append(string2numeric_hash(sourceParty.encode('utf-8')))
            #             # START DATE
            #             if 'startDate' in data["records"][0]["compiledRelease"]["planning"]["budget"]["budgetBreakdown"][i]["period"]:
            #                 startDate = data["records"][0]["compiledRelease"]["planning"]["budget"]["budgetBreakdown"][i]["period"]["startDate"]
            #                 startDate = datetime.strptime(startDate, '%Y-%m-%dT%H:%M:%SZ')
            #                 # END DATE
            #                 if 'endDate' in data["records"][0]["compiledRelease"]["planning"]["budget"]["budgetBreakdown"][i]["period"]:
            #                     endDate = \
            #                     data["records"][0]["compiledRelease"]["planning"]["budget"]["budgetBreakdown"][i]["period"]["endDate"]
            #                     endDate = datetime.strptime(endDate, '%Y-%m-%dT%H:%M:%SZ')
            #                     period = endDate - startDate
            #                     period = period.total_seconds()
            #                     appendNameValue(period, 'period')
            #                 # maxExtentDate
            #                 if 'maxExtentDate' in data["records"][0]["compiledRelease"]["planning"]["budget"]["budgetBreakdown"][i]["period"]:
            #                     maxExtentDate = data["records"][0]["compiledRelease"]["planning"]["budget"]["budgetBreakdown"][i]["period"]["maxExtentDate"]
            #                     maxExtentDate = datetime.strptime(maxExtentDate, '%Y-%m-%dT%H:%M:%SZ')
            #                     period = maxExtentDate - startDate
            #                     period = period.total_seconds()
            #                     appendNameValue(period, 'maxExtentPeriod')
            #
            # appendNameValue(planning_budget_budgetBreakdown_amount, 'planning_budget_budgetBreakdown_amount')
            # appendNameValue(planning_budget_budgetBreakdown_commitedAmount, 'planning_budget_budgetBreakdown_commitedAmount')
            # appendNameValue(planning_budget_budgetBreakdown_status, 'planning_budget_budgetBreakdown_status')
            # appendNameValue(planning_budget_budgetBreakdown_availabilityCertificateNumber, 'planning_budget_budgetBreakdown_availabilityCertificateNumber')
            # appendNameValue(planning_budget_budgetBreakdown_sourceParty, 'planning_budget_budgetBreakdown_sourceParty')

            # DOCUMENTS
            planning_documents_id = []
            planning_documents_format = []
            planning_documents_language = []
            planning_documents_documentType = []
            if 'documents' in data['records'][0]['compiledRelease']['planning']:
                for i in range(len(data["records"][0]["compiledRelease"]["planning"]["documents"])):
                    # ID
                    data_id = data["records"][0]["compiledRelease"]["planning"]["documents"][i]["id"]
                    planning_documents_id.append(string2numeric_hash(data_id.encode('utf-8')))
                    # FORMAT
                    data_format = data["records"][0]["compiledRelease"]["planning"]["documents"][i]["format"]
                    planning_documents_format.append(format_switcher.get(data_format, 0))
                    # LANGUAGE
                    language = data["records"][0]["compiledRelease"]["planning"]["documents"][i]["language"]
                    planning_documents_language.append(language_switcher.get(language, 0))
                    # DOCUMENT TYPE
                    documentType = data["records"][0]["compiledRelease"]["planning"]["documents"][i]["documentType"]
                    planning_documents_documentType.append(documentType_switcher.get(documentType, 0))
            appendNameValue(planning_documents_id, 'planning_documents_id')
            appendNameValue(planning_documents_format, 'planning_documents_format')
            appendNameValue(planning_documents_language, 'planning_documents_language')
            appendNameValue(planning_documents_documentType, 'planning_documents_documentType')

        # PARTIES
        if 'parties' in data['records'][0]['compiledRelease']:
            for i in range(len(data["records"][0]["compiledRelease"]["parties"])):
                # ID
                data_id = data["records"][0]["compiledRelease"]["parties"][i]["id"]
                appendNameValue(string2numeric_hash(data_id.encode('utf-8')), 'parties_id')
                # NAME
                data_id = data["records"][0]["compiledRelease"]["parties"][i]["name"]
                appendNameValue(string2numeric_hash(data_id.encode('utf-8')), 'parties_name')
                if 'contactPoint' in data['records'][0]['compiledRelease']["parties"][i]:
                    if 'name' in data['records'][0]['compiledRelease']["parties"][i]["contactPoint"]:
                        data_id = data["records"][0]["compiledRelease"]["parties"][i]["contactPoint"]["name"]
                        appendNameValue(string2numeric_hash(data_id.encode('utf-8')), 'parties_contactPoint_name')
                    if 'email' in data['records'][0]['compiledRelease']["parties"][i]["contactPoint"]:
                        data_id = data["records"][0]["compiledRelease"]["parties"][i]["contactPoint"]["email"]
                        appendNameValue(string2numeric_hash(data_id.encode('utf-8')), 'parties_contactPoint_email')
                    if 'url' in data['records'][0]['compiledRelease']["parties"][i]["contactPoint"]:
                        data_id = data["records"][0]["compiledRelease"]["parties"][i]["contactPoint"]["url"]
                        if data_id is not None:
                            appendNameValue(string2numeric_hash(data_id.encode('utf-8')), 'parties_contactPoint_url')
                    if 'telephone' in data['records'][0]['compiledRelease']["parties"][i]["contactPoint"]:
                        data_id = data["records"][0]["compiledRelease"]["parties"][i]["contactPoint"]["telephone"]
                        appendNameValue(removeDividingCharacters(data_id), 'parties_contactPoint_telephone')
                if 'identifier' in data['records'][0]['compiledRelease']["parties"][i]:
                    if 'id' in data['records'][0]['compiledRelease']["parties"][i]["identifier"]:
                        data_id = data["records"][0]["compiledRelease"]["parties"][i]["identifier"]["id"]
                        appendNameValue(string2numeric_hash(data_id.encode('utf-8')), 'parties_identifier_id')
                    if 'scheme' in data['records'][0]['compiledRelease']["parties"][i]["identifier"]:
                        data_id = data["records"][0]["compiledRelease"]["parties"][i]["identifier"]["scheme"]
                        appendNameValue(scheme_switcher.get(data_id, 0), 'parties_identifier_scheme')
                    if 'legalName' in data['records'][0]['compiledRelease']["parties"][i]["identifier"]:
                        data_id = data["records"][0]["compiledRelease"]["parties"][i]["identifier"]["legalName"]
                        appendNameValue(string2numeric_hash(data_id.encode('utf-8')), 'parties_identifier_legalName')
                if 'roles' in data['records'][0]['compiledRelease']["parties"][i]:
                    roles = []
                    for rol in data["records"][0]["compiledRelease"]["parties"][i]["roles"]:
                        roles.append(roles_switcher.get(rol, 0))
                    appendNameValue(roles, 'parties_roles')
                # if 'address' in data['records'][0]['compiledRelease']["parties"][i]:
                    # if 'region' in data['records'][0]['compiledRelease']["parties"][i]["address"]:
                    #     data_id = data["records"][0]["compiledRelease"]["parties"][i]["address"]["region"]
                    #     appendNameValue(string2numeric_hash(data_id.encode('utf-8')),
                    #                     'parties_address_region')
                    # if 'locality' in data['records'][0]['compiledRelease']["parties"][i]["address"]:
                    #     data_id = data["records"][0]["compiledRelease"]["parties"][i]["address"]["locality"]
                    #     appendNameValue(string2numeric_hash(data_id.encode('utf-8')),
                    #                     'parties_address_locality')
                    # if 'streetAddress' in data['records'][0]['compiledRelease']["parties"][i]["address"]:
                    #     data_id = data["records"][0]["compiledRelease"]["parties"][i]["address"]["streetAddress"]
                    #     appendNameValue(string2numeric_hash(data_id.encode('utf-8')),
                    #                     'parties_address_streetAddress')
                    # if 'countryName' in data['records'][0]['compiledRelease']["parties"][i]["address"]:
                    #     data_id = data["records"][0]["compiledRelease"]["parties"][i]["address"]["countryName"]
                    #     appendNameValue(country_switcher.get(data_id, 0), 'parties_address_countryName')
                if 'memberOf' in data['records'][0]['compiledRelease']["parties"][i]:
                    memberOf = []
                    for member in data["records"][0]["compiledRelease"]["parties"][i]["memberOf"]:
                        memberOf.append(string2numeric_hash(member["id"].encode('utf-8')))
                    appendNameValue(memberOf, 'parties_memberOf')
        # TAG
        # for tag in data['records'][0]['compiledRelease']['tag']:
        #     appendNameValue(tag_switcher.get(tag, 0), 'tag')
        # # INITIATION TYPE
        # initiationType = data['records'][0]['compiledRelease']['initiationType']
        # appendNameValue(tag_switcher.get(initiationType, 0), 'initiationType')
        # # AWARDS
        # if 'awards' in data["records"][0]["compiledRelease"]:
        #     for award in data["records"][0]["compiledRelease"]["awards"]:
        #         # STATUS
        #         if 'status' in award:
        #             status = award["status"]
        #             appendNameValue(award_status_switcher.get(status, 0), 'award_status')
        #         # ID
        #         if 'id' in award:
        #             data_id = award["id"]
        #             appendNameValue(string2numeric_hash(data_id.encode('utf-8')), 'award_id')
        #         # AMOUNT
        #         if 'value' in award:
        #             amount = award["value"]["amount"]
        #             currency = award["value"]["currency"]
        #             exchange_rate = getExchangeRate(currency)
        #             data_id = amount * exchange_rate
        #             appendNameValue(data_id, 'award_amount')
        #         if 'date' in award:
        #             award_date = award["date"]
        #             award_date = datetime.strptime(award_date, '%Y-%m-%dT%H:%M:%SZ')
        #             period = award_date - date
        #             period = period.total_seconds()
        #             appendNameValue(period, 'award_period')
        #         if 'suppliers' in award:
        #             for supplier in award['suppliers']:
        #                 # ID
        #                 if 'id' in supplier:
        #                     data_id = supplier["id"]
        #                     appendNameValue(string2numeric_hash(data_id.encode('utf-8')), 'award_supplier_id')
        #                 # NAME
        #                 if 'name' in supplier:
        #                     data_id = supplier["name"]
        #                     appendNameValue(string2numeric_hash(data_id.encode('utf-8')), 'award_supplier_name')
        # CONTRACTS
        if 'contracts' in data["records"][0]["compiledRelease"]:
            for contract in data["records"][0]["compiledRelease"]["contracts"]:
                # # STATUS
                # if 'status' in contract:
                #     status = contract["status"]
                #     appendNameValue(contract_status_switcher.get(status, 0), 'contract_status')
                # # ID
                # if 'id' in contract:
                #     data_id = contract["id"]
                #     appendNameValue(string2numeric_hash(data_id.encode('utf-8')), 'contract_id')
                # # AMOUNT
                # if 'value' in contract:
                #     amount = contract["value"]["amount"]
                #     currency = contract["value"]["currency"]
                #     exchange_rate = getExchangeRate(currency)
                #     data_id = amount * exchange_rate
                #     appendNameValue(data_id, 'contract_amount')
                # # dncpContractCode
                # if 'dncpContractCode' in contract:
                #     data_id = contract["dncpContractCode"]
                #     appendNameValue(string2numeric_hash(data_id.encode('utf-8')), 'contract_dncpContractCode')
                # # awardID
                # if 'awardID' in contract:
                #     data_id = contract["awardID"]
                #     appendNameValue(string2numeric_hash(data_id.encode('utf-8')), 'contract_awardID')
                # DOCUMENTS
                if 'documents' in contract:
                    for document in contract['documents']:
                        # ID
                        if 'id' in document:
                            data_id = document["id"]
                            appendNameValue(string2numeric_hash(data_id.encode('utf-8')), 'contract_document_id')
                        # FORMAT
                        if 'format' in contract:
                            status = contract["format"]
                            appendNameValue(format_switcher.get(status, 0), 'contract_document_format')
                        # documentType
                        if 'documentType' in contract:
                            status = contract["documentType"]
                            appendNameValue(documentType_switcher.get(status, 0), 'contract_document_documentType')
                # ITEMS
                # if 'items' in contract:
                #     for item in contract['documents']:
                #         # ID
                #         if 'id' in item:
                #             data_id = item["id"]
                #             appendNameValue(string2numeric_hash(data_id.encode('utf-8')), 'contract_item_id')
                #         # Quantity
                #         if 'quantity' in item:
                #             status = item["quantity"]
                #             appendNameValue(status, 'contract_item_quantity')
                #         # UNIT
                #         if 'unit' in item:
                #             # NAME
                #             if 'name' in item['unit']:
                #                 data_id = item['unit']['name']
                #                 appendNameValue(unit_name_switcher.get(data_id, 0), 'contract_item_unit')
                #             # AMOUNT
                #             if 'value' in item['unit']:
                #                 amount = item['unit']["value"]["amount"]
                #                 currency = item['unit']["value"]["currency"]
                #                 exchange_rate = getExchangeRate(currency)
                #                 data_id = amount * exchange_rate
                #                 appendNameValue(data_id, 'contract_item_unit_amount')
                #         # classification
                #         if 'classification' in item:
                #             if 'id' in item['classification']:
                #                 data_id = item['classification']["id"]
                #                 appendNameValue(string2numeric_hash(data_id.encode('utf-8')),
                #                                 'contract_item_classification_id')
                # # IMPLEMENTATION
                # if 'implementation' in contract:
                #     for transaction in contract['implementation']['transactions']:
                #         # ID
                #         if 'id' in transaction:
                #             data_id = transaction["id"]
                #             appendNameValue(string2numeric_hash(data_id.encode('utf-8')),
                #                             'contract_implementation_transaction_id')
                        # if 'date' in transaction:
                        #     transaction_date = transaction["date"]
                        #     transaction_date = datetime.strptime(transaction_date, '%Y-%m-%dT%H:%M:%SZ')

                        # if 'requestDate' in paymentRequest:
                        #     transaction_requestDate = paymentRequest["requestDate"]
                        #     transaction_requestDate = datetime.strptime(transaction_requestDate,
                        #                                                  '%Y-%m-%dT%H:%M:%SZ')
                        #     period = transaction_requestDate - transaction_date
                        #     period = period.total_seconds()
                        #     appendNameValue(period, 'transaction_requestDate_period')
                        # # PAYEE
                        # if 'payee' in transaction:
                        #     if 'id' in transaction['payee']:
                        #         data_id = transaction['payee']["id"]
                        #         appendNameValue(string2numeric_hash(data_id.encode('utf-8')),
                        #                         'contract_implementation_transaction_payee_id')
                        #     if 'name' in transaction['payee']:
                        #         data_id = transaction['payee']["name"]
                        #         appendNameValue(string2numeric_hash(data_id.encode('utf-8')),
                        #                         'contract_implementation_transaction_payee_name')
                        # # PAYER
                        # if 'payer' in transaction:
                        #     if 'id' in transaction['payer']:
                        #         data_id = transaction['payer']["id"]
                        #         appendNameValue(string2numeric_hash(data_id.encode('utf-8')),
                        #                         'contract_implementation_transaction_payer_id')
                        #     if 'name' in transaction['payer']:
                        #         data_id = transaction['payer']["name"]
                        #         appendNameValue(string2numeric_hash(data_id.encode('utf-8')),
                        #                         'contract_implementation_transaction_payer_name')
                        # # AMOUNT
                        # if 'value' in transaction:
                        #     amount = transaction["value"]["amount"]
                        #     currency = transaction["value"]["currency"]
                        #     exchange_rate = getExchangeRate(currency)
                        #     data_id = amount * exchange_rate
                        #     appendNameValue(data_id, 'contract_implementation_transaction_amount')
                        # # finantialObligations
                        # if 'finantialObligations' in transaction:
                        #     for finantialObligation in transaction['finantialObligations']:
                        #         if 'id' in finantialObligation:
                        #             data_id = finantialObligation["id"]
                        #             appendNameValue(string2numeric_hash(data_id.encode('utf-8')),
                        #                             'contract_implementation_transaction_finantialObligation_id')
                        #         if 'creationDate' in finantialObligation:
                        #             transaction_creationDate = finantialObligation["creationDate"]
                        #             transaction_creationDate = datetime.strptime(transaction_creationDate,
                        #                                                       '%Y-%m-%dT%H:%M:%SZ')
                        #             period = transaction_creationDate - transaction_date
                        #             period = period.total_seconds()
                        #             appendNameValue(period, 'transaction_finantialObligation_creationDate_period')
                        #             if 'approvalDate' in finantialObligation:
                        #                 transaction_approvalDate = finantialObligation["approvalDate"]
                        #                 transaction_approvalDate = datetime.strptime(transaction_approvalDate,
                        #                                                           '%Y-%m-%dT%H:%M:%SZ')
                        #                 period = transaction_approvalDate - transaction_creationDate
                        #                 period = period.total_seconds()
                        #                 appendNameValue(period, 'transaction_finantialObligation_approvalDate_period')
                        #             if 'modificationDate' in finantialObligation:
                        #                 transaction_modificationDate = finantialObligation["modificationDate"]
                        #                 transaction_modificationDate = datetime.strptime(transaction_modificationDate,
                        #                                                           '%Y-%m-%dT%H:%M:%SZ')
                        #                 period = transaction_modificationDate - transaction_creationDate
                        #                 period = period.total_seconds()
                        #                 appendNameValue(period, 'transaction_finantialObligation_modificationDate_period')
                        #             if 'cancellationDate' in finantialObligation:
                        #                 transaction_cancellationDate = finantialObligation["cancellationDate"]
                        #                 transaction_cancellationDate = datetime.strptime(transaction_cancellationDate,
                        #                                                           '%Y-%m-%dT%H:%M:%SZ')
                        #                 period = transaction_cancellationDate - transaction_creationDate
                        #                 period = period.total_seconds()
                        #                 appendNameValue(period, 'transaction_finantialObligation_cancellationDate_period')
                        #         if 'bill' in finantialObligation:
                        #             if 'id' in finantialObligation['bill']:
                        #                 data_id = finantialObligation['bill']["id"]
                        #                 appendNameValue(string2numeric_hash(data_id.encode('utf-8')),
                        #                                 'contract_implementation_transaction_finantialObligation_bill_id')
                        #             if 'date' in finantialObligation['bill']:
                        #                 transaction_bill_date = finantialObligation['bill']["date"]
                        #                 transaction_bill_date = datetime.strptime(transaction_bill_date, '%Y-%m-%dT%H:%M:%SZ')
                        #                 period = transaction_bill_date - transaction_date
                        #                 period = period.total_seconds()
                        #                 appendNameValue(period, 'transaction_bill_period')
                        #             if 'amount' in finantialObligation['bill']:
                        #                 amount = finantialObligation['bill']["amount"]["amount"]
                        #                 currency = finantialObligation['bill']["amount"]["currency"]
                        #                 exchange_rate = getExchangeRate(currency)
                        #                 data_id = amount * exchange_rate
                        #                 appendNameValue(data_id, 'contract_implementation_transaction_finantialObligation_bill_amount')
                        #             if 'type' in finantialObligation['bill']:
                        #                 status = finantialObligation['bill']["type"]
                        #                 appendNameValue(bill_type_switcher.get(status, 0), 'contract_implementation_transaction_finantialObligation_bill_type')
                        #         if 'retentions' in finantialObligation:
                        #             for retention in finantialObligation['retentions']:
                        #                 if 'amount' in retention:
                        #                     amount = retention["amount"]["amount"]
                        #                     currency = retention["amount"]["currency"]
                        #                     exchange_rate = getExchangeRate(currency)
                        #                     data_id = amount * exchange_rate
                        #                     appendNameValue(data_id,
                        #                                     'contract_implementation_transaction_finantialObligation_retention_amount')
                        #                 if 'type' in retention:
                        #                     status = retention["type"]
                        #                     appendNameValue(retention_type_switcher.get(status, 0),
                        #                                     'contract_implementation_transaction_finantialObligation_retention_type')
                        # # paymentRequests
                        # if 'paymentRequests' in transaction:
                        #     for paymentRequest in transaction['paymentRequests']:
                        #         if 'id' in paymentRequest:
                        #             data_id = paymentRequest["id"]
                        #             appendNameValue(string2numeric_hash(data_id.encode('utf-8')),
                        #                             'contract_implementation_transaction_paymentRequest_id')
                        #         if 'totalRetentionAmount' in paymentRequest:
                        #             amount = paymentRequest["totalRetentionAmount"]["amount"]
                        #             currency = paymentRequest["totalRetentionAmount"]["currency"]
                        #             exchange_rate = getExchangeRate(currency)
                        #             data_id = amount * exchange_rate
                        #             appendNameValue(data_id, 'contract_implementation_transaction_paymentRequest_totalRetentionAmount')
                        #         if 'status' in paymentRequest:
                        #             status = paymentRequest["status"]
                        #             appendNameValue(implementation_status.get(status, 0),
                        #                             'contract_implementation_transaction_paymentRequest_status')
                        #         if 'payedAmount' in paymentRequest:
                        #             amount = paymentRequest["payedAmount"]["amount"]
                        #             currency = paymentRequest["payedAmount"]["currency"]
                        #             exchange_rate = getExchangeRate(currency)
                        #             data_id = amount * exchange_rate
                        #             appendNameValue(data_id, 'contract_implementation_transaction_paymentRequest_payedAmount')
                        #         if 'requestedAmount' in paymentRequest:
                        #             amount = paymentRequest["requestedAmount"]["amount"]
                        #             currency = paymentRequest["requestedAmount"]["currency"]
                        #             exchange_rate = getExchangeRate(currency)
                        #             data_id = amount * exchange_rate
                        #             appendNameValue(data_id, 'contract_implementation_transaction_paymentRequest_requestedAmount')
                        #         if 'creationDate' in paymentRequest:
                        #             transaction_creationDate = paymentRequest["creationDate"]
                        #             transaction_creationDate = datetime.strptime(transaction_creationDate,
                        #                                                       '%Y-%m-%dT%H:%M:%SZ')
                        #             period = transaction_creationDate - transaction_date
                        #             period = period.total_seconds()
                        #             appendNameValue(period, 'transaction_paymentRequest_creationDate_period')
                        #             if 'treasuryReceptionDate' in paymentRequest:
                        #                 transaction_approvalDate = paymentRequest["treasuryReceptionDate"]
                        #                 transaction_approvalDate = datetime.strptime(transaction_approvalDate,
                        #                                                           '%Y-%m-%dT%H:%M:%SZ')
                        #                 period = transaction_approvalDate - transaction_creationDate
                        #                 period = period.total_seconds()
                        #                 appendNameValue(period, 'transaction_paymentRequest_treasuryReceptionDate_period')
                        #             if 'modificationDate' in paymentRequest:
                        #                 transaction_modificationDate = paymentRequest["modificationDate"]
                        #                 transaction_modificationDate = datetime.strptime(transaction_modificationDate,
                        #                                                           '%Y-%m-%dT%H:%M:%SZ')
                        #                 period = transaction_modificationDate - transaction_creationDate
                        #                 period = period.total_seconds()
                        #                 appendNameValue(period, 'transaction_paymentRequest_modificationDate_period')
                        #             if 'authorizationDate' in paymentRequest:
                        #                 transaction_cancellationDate = paymentRequest["authorizationDate"]
                        #                 transaction_cancellationDate = datetime.strptime(transaction_cancellationDate,
                        #                                                           '%Y-%m-%dT%H:%M:%SZ')
                        #                 period = transaction_cancellationDate - transaction_creationDate
                        #                 period = period.total_seconds()
                        #                 appendNameValue(period, 'transaction_paymentRequest_authorizationDate_period')

        # # TENDER
        # if 'tender' in data["records"][0]["compiledRelease"]:
        #     tender = data["records"][0]["compiledRelease"]["tender"]
        #     if 'id' in tender:
        #         data_id = tender["id"]
        #         appendNameValue(string2numeric_hash(data_id.encode('utf-8')), 'tender_id')
        #     if 'status' in tender:
        #         status = tender["status"]
        #         appendNameValue(tender_status.get(status, 0), 'tender_status')
        #     if 'submissionMethod' in tender:
        #         for method in tender["submissionMethod"]:
        #             status = method
        #             appendNameValue(submission_methods.get(status, 0), 'tender_submissionMethod')
        #     if 'procurementMethodDetails' in tender:
        #         status = tender["procurementMethodDetails"]
        #         appendNameValue(procurement_method_details.get(status, 0), 'tender_procurementMethodDetails')
        #     if 'documents' in tender:
        #         for document in tender['documents']:
        #             if 'id' in document:
        #                 data_id = document["id"]
        #                 appendNameValue(string2numeric_hash(data_id.encode('utf-8')), 'tender_document_id')
        #             if 'format' in document:
        #                 status = document["format"]
        #                 appendNameValue(format_switcher.get(status, 0), 'tender_document_format')
        #             if 'language' in document:
        #                 status = document["language"]
        #                 appendNameValue(language_switcher.get(status, 0), 'tender_document_language')
        #             if 'documentType' in document:
        #                 status = document["documentType"]
        #                 appendNameValue(documentType_switcher.get(status, 0), 'tender_document_documentType')
        #     if 'hasEnquiries' in tender:
        #         status = tender["hasEnquiries"]
        #         if status:
        #             appendNameValue(1, 'tender_hasEnquiries')
        #         else:
        #             appendNameValue(0, 'tender_hasEnquiries')
        #     if 'procuringEntity' in tender:
        #         if 'id' in tender['procuringEntity']:
        #             data_id = tender['procuringEntity']["id"]
        #             appendNameValue(string2numeric_hash(data_id.encode('utf-8')), 'tender_procuringEntity_id')
        #         if 'name' in tender['procuringEntity']:
        #             data_id = tender['procuringEntity']["name"]
        #             appendNameValue(string2numeric_hash(data_id.encode('utf-8')), 'tender_procuringEntity_name')
        #     if 'tenderers' in tender:
        #         for tenderer in tender['tenderers']:
        #             if 'id' in tenderer:
        #                 data_id = tenderer["id"]
        #                 appendNameValue(string2numeric_hash(data_id.encode('utf-8')), 'tender_tenderer_id')
        #             if 'name' in tenderer:
        #                 data_id = tenderer["name"]
        #                 appendNameValue(string2numeric_hash(data_id.encode('utf-8')), 'tender_tenderer_name')
        #     if 'numberOfTenderers' in tender:
        #         data_id = tender["numberOfTenderers"]
        #         appendNameValue(data_id, 'tender_numberOfTenderers')
        #     if 'value' in tender:
        #         amount = tender["value"]["amount"]
        #         currency = tender["value"]["currency"]
        #         exchange_rate = getExchangeRate(currency)
        #         if moneda is not None and amount is not None:
        #             data_id = amount * exchange_rate
        #             appendNameValue(data_id, 'tender_amount')
        #     if 'items' in tender:
        #         for item in tender['items']:
        #             if 'id' in item:
        #                 data_id = item["id"]
        #                 appendNameValue(string2numeric_hash(data_id.encode('utf-8')), 'tender_item_id')
        #             if 'quantity' in item:
        #                 print(item)
        #                 data_id = item['quantity']
        #                 appendNameValue(data_id, 'tender_item_quantity')
        #             if 'classification' in item:
        #                 if 'id' in item['classification']:
        #                     data_id = item['classification']["id"]
        #                     appendNameValue(string2numeric_hash(data_id.encode('utf-8')),
        #                                     'tender_item_classification_id')
        #             if 'unit' in item:
        #                 if 'name' in item['unit']:
        #                     status = item['unit']['name']
        #                     appendNameValue(unit_name_switcher.get(status, 0), 'tender_items_unit_name')
        #                 if 'value' in item['unit']:
        #                     amount = item['unit']["value"]["amount"]
        #                     currency = item['unit']["value"]["currency"]
        #                     exchange_rate = getExchangeRate(currency)
        #                     if moneda is not None and amount is not None:
        #                         data_id = amount * exchange_rate
        #                         appendNameValue(data_id, 'tender_unit_amount')
        #     if 'tenderPeriod' in tender:
        #         if 'startDate' in tender['tenderPeriod']:
        #             tenderPeriod_startDate = tender['tenderPeriod']["startDate"]
        #             if tenderPeriod_startDate is not None:
        #                 tenderPeriod_startDate = datetime.strptime(tenderPeriod_startDate, '%Y-%m-%dT%H:%M:%SZ')
        #                 if 'endDate' in tender['tenderPeriod']:
        #                     tenderPeriod_endDate = tender['tenderPeriod']["endDate"]
        #                     tenderPeriod_endDate = datetime.strptime(tenderPeriod_endDate, '%Y-%m-%dT%H:%M:%SZ')
        #                 period = tenderPeriod_endDate - tenderPeriod_startDate
        #                 period = period.total_seconds()
        #                 appendNameValue(period, 'tender_tenderPeriod')
        #                 if 'maxExtentDate' in tender['tenderPeriod']:
        #                     tenderPeriod_maxExtentDate = tender['tenderPeriod']["maxExtentDate"]
        #                     tenderPeriod_maxExtentDate = datetime.strptime(tenderPeriod_maxExtentDate, '%Y-%m-%dT%H:%M:%SZ')
        #                     period = tenderPeriod_maxExtentDate - tenderPeriod_startDate
        #                     period = period.total_seconds()
        #                     appendNameValue(period, 'tender_tenderPeriod_maxExtentDate')
        #     if 'enquiryPeriod' in tender:
        #         if 'startDate' in tender['enquiryPeriod']:
        #             enquiryPeriod_startDate = tender['enquiryPeriod']["startDate"]
        #             enquiryPeriod_startDate = datetime.strptime(enquiryPeriod_startDate, '%Y-%m-%dT%H:%M:%SZ')
        #             if 'endDate' in tender['enquiryPeriod']:
        #                 enquiryPeriod_endDate = tender['enquiryPeriod']["endDate"]
        #                 enquiryPeriod_endDate = datetime.strptime(enquiryPeriod_endDate, '%Y-%m-%dT%H:%M:%SZ')
        #             period = enquiryPeriod_endDate - enquiryPeriod_startDate
        #             period = period.total_seconds()
        #             appendNameValue(period, 'tender_enquiryPeriod')
        #     if 'awardPeriod' in tender:
        #         if 'startDate' in tender['awardPeriod']:
        #             awardPeriod_startDate = tender['awardPeriod']["startDate"]
        #             awardPeriod_startDate = datetime.strptime(awardPeriod_startDate, '%Y-%m-%dT%H:%M:%SZ')
        #             if 'endDate' in tender['awardPeriod']:
        #                 awardPeriod_endDate = tender['awardPeriod']["endDate"]
        #                 if awardPeriod_endDate is not None:
        #                     awardPeriod_endDate = datetime.strptime(awardPeriod_endDate, '%Y-%m-%dT%H:%M:%SZ')
        #                     period = awardPeriod_endDate - awardPeriod_startDate
        #                     period = period.total_seconds()
        #                     appendNameValue(period, 'tender_awardPeriod')
        #
        # # BUYER
        # if 'buyer' in data["records"][0]["compiledRelease"]:
        #     buyer = data["records"][0]["compiledRelease"]["buyer"]
        #     if 'id' in buyer:
        #         data_id = buyer["id"]
        #         appendNameValue(string2numeric_hash(data_id.encode('utf-8')), 'buyer_id')
        #     if 'name' in buyer:
        #         data_id = buyer["name"]
        #         appendNameValue(string2numeric_hash(data_id.encode('utf-8')), 'buyer_name')
        rows.append(row)
        title_rows.append(title_row)
        row = []
        title_row = []

    print('sort')
    rows.sort(key=len, reverse=True)
    title_rows.sort(key=len, reverse=True)
    print('compare lists')
    compare_lists(0, 1)
    # for i in range(len(rows) - 1):
    #     compare_lists(0, i + 1)
    # for i in range(len(rows)):
    #     print(len(title_rows[i]))
    #     print(len(rows[i]))
        # rows[i] = convertToCSV(rows[i])
    with open('/home/maeli/PycharmProjects/change_version_1_1_ocds/pruebas_datos/converted.csv', 'w') as outfile:
        for i in rows:
            outfile.write(str(i))
            outfile.write("\n")


convertToNumeric()