import psycopg2
import os
import json


def save_to_postgreSQL():
    indir = '/home/mekehler/Documentos/contratos_change_version/'
    for root, dirs, filenames in os.walk(indir):
        for f in filenames:
            with open(indir + f) as json_data:
                data = json.load(json_data)
            try:
                print(f[:6])
                print(data)
                conn = psycopg2.connect(dbname="maeli", user="tesis", password="admin_tesis")
                cursor = conn.cursor()
                query = 'INSERT INTO contratos_v1_1_etiqueta_tenderers (id, data) VALUES (%s, %s);'
                d = (f[:6], json.dumps(data, indent=2, ensure_ascii=False))
                cursor.execute(query, d)
                conn.commit()
                cursor.close()
                del cursor
                conn.close()
                del conn

            except Exception as e:
                if str(e)[:13] != "duplicate key":
                    with open("err_dato_validados_a_db.txt", 'a') as err:
                        print(' Error: ' + str(e))
                        err.write(str(f) + "\n")


save_to_postgreSQL()
