from convert_data.getExchangeRate import getExchangeRate
from convert_data.convert_string import hashValue
from convert_data.convert_string import convertArray
from convert_data.convert_string import appendNameValue


# planning date????
def planning(data, moneda_date, title_row, row):
    if 'etiquettes' in data['records'][0]['compiledRelease']['planning']:
        data_ettiquetes = data["records"][0]["compiledRelease"]["planning"]["etiquettes"]
        planning_etiquetes = convertArray(data_ettiquetes, 'etiquettes')
        appendNameValue(planning_etiquetes, 'planning_etiquettes', title_row, row, None)
    if 'budget' in data["records"][0]["compiledRelease"]["planning"]:
        moneda = data["records"][0]["compiledRelease"]["planning"]["budget"]["amount"]["currency"]
        amount = data["records"][0]["compiledRelease"]["planning"]["budget"]["amount"]["amount"]
        if moneda is not None and amount is not None:
            exchange_rate = getExchangeRate(moneda, moneda_date)
            planning_budget_amount = amount*exchange_rate
            appendNameValue(planning_budget_amount, 'planning_budget_amount', title_row, row, None)
            # planning_budget_currency = convertArray(moneda, 'currency')
            # appendNameValue(planning_budget_currency, 'planning_budget_currency', title_row, row, None)
        if 'project' in data["records"][0]["compiledRelease"]["planning"]["budget"]:
            planning_budget_project = hashValue(data["records"][0]["compiledRelease"]["planning"]["budget"]['project'],
                                                False, True)
            appendNameValue(planning_budget_project, 'planning_budget_project', title_row, row, None)
    aux_row = []
    if 'documents' in data['records'][0]['compiledRelease']['planning']:
        for document in data['records'][0]['compiledRelease']['planning']['documents']:
            if 'documentType' in document:
                aux_row.append(document['documentType'])
        document_type = convertArray(aux_row, 'planningDocumentType')
        appendNameValue(document_type, 'planning_documentTypes', title_row, row, None)
    return [title_row, row]
