import json
import os
import csv
from copy import copy, deepcopy
from datetime import datetime
import convert_data.parties as parties
from convert_data.planning import planning
from convert_data.awards import awards
from convert_data.contracts import contracts
from convert_data.tender import tender
from convert_data.diccionario import initialize_dict
from convert_data.convert_string import hashValue
from convert_data.convert_string import appendNameValue
import convert_data.diccionario as diccionario
from convert_data.getExchangeRate import getExchangeRate
import psycopg2

counter = -1
title_row = []
data_rows = {}
contract_data_rows = {}
contract_title_rows = {}
no_contract_counter = 0
end_title_row = ['id', 'ocid', 'planning_etiquettes', 'planning_budget_amount', 'planning_budget_project', 'planning_documentTypes',

                 'tender_id', 'tenderPeriod_startDateMonth', 'tenderPeriod_endDateMonth', 'tenderPeriod_maxExtentDateMonth',
                 'tenderPeriod_period', 'tenderPeriod_maxExtentPeriod', 'tender_amount',
                 'tender_enquiryPeriod_startDateMonth', 'tender_enquiryPeriod_endDateMonth',
                 'tender_enquiryPeriod_period', 'tender_awardPeriod_startDateMonth', 'tender_awardPeriod_endDateMonth',
                 'tender_awardPeriod_period', 'tender_hasEnquiries', 'tender_procuringEntity_identifier',
                 'tender_procuringEntity_contactPoint_email', 'tender_procuringEntity_memberOf',
                 'tender_procuringEntity_roles', 'tender_procurementMethodDetails', 'tender_documentTypes',]


def convertToNumeric():
    global title_row
    global data_rows
    global counter
    global contract_data_rows
    global contract_title_rows
    global no_contract_counter
    global end_title_row
    contract_title_rows = {}
    contract_data_rows = {}
    outfile_dir = 'convertedData/converted_tender_electronicAuction.csv'
    dic_dir = 'convertedData/name_diccionario_tender_electronicAuction.json'
    initialize_dict(dic_dir)
    conn = psycopg2.connect(dbname="maeli", user="tesis", password="admin_tesis")
    cursor = conn.cursor()
    # electronicAuction or written
    query = "SELECT id, data from contratos_v1_1_etiqueta_tenderers_hacienda_2019 " \
            "where data->'records'->0->'compiledRelease'->'tender'->'submissionMethod'->> 0 like 'electronicAuction';"
    cursor.execute(query)
    datos = cursor.fetchall()
    cursor.close()
    del cursor
    conn.close()
    del conn
    print('end query')
    print('num dimensions: ' + str(len(end_title_row)))
    for registro in datos:
        try:
            print(str(registro[0]))
            data_rows = []
            counter = -1
            data_rows = {}
            row = []
            title_row = []
            data = registro[1]
            moneda_date = None
            if 'compiledRelease' in data["records"][0]:
                ocid = hashValue(data["records"][0]["compiledRelease"]['ocid'], False, True)
                title_row.append('id')
                row.append(int(registro[0]))
                title_row.append('ocid')
                row.append(ocid)
                if 'parties' in data["records"][0]["compiledRelease"]:
                    parties.parties(data)
                if 'planning' in data["records"][0]["compiledRelease"]:
                    planning_title_row = []
                    planning_row = []
                    [planning_title_row, planning_row] = planning(data, moneda_date, planning_title_row, planning_row)
                    for i in range(len(planning_title_row)):
                        appendNameValue(planning_row[i], planning_title_row[i], title_row, row, None)
                    counter = counter + 1
                    data_rows[counter] = row
                    aux_data_rows = {}
                    for i in data_rows:
                        aux_data_rows[i] = copy(data_rows[i])

                if 'tender' in data["records"][0]["compiledRelease"]:
                    tender_title_row = []
                    tender_row = []
                    [tender_id, tender_title_row, tender_row, moneda_date, tender_end_date] = \
                        tender(data["records"][0]["compiledRelease"]['tender'], tender_title_row, tender_row)
                    for i in range(len(tender_title_row)):
                        appendNameValue(tender_row[i], tender_title_row[i], title_row, row, None)
                    data_rows[counter] = row

                for i in range(len(data_rows)):
                    if len(title_row) != len(data_rows[i]):
                        print('datarows')
                        print(len(title_row))
                        print(len(data_rows[i]))

                contract_data_rows[registro[0]] = data_rows
                contract_title_rows[registro[0]] = title_row
            else:
                with open("no_compiledRelease.txt", 'a') as err:
                    err.write(str(registro[0]) + "\n")
        except Exception as e:
            print(e)

    print('proceso terminado')
    keys = list(contract_data_rows.keys())
    new_contract_datarow = []
    for i in range(len(end_title_row)):
        new_contract_datarow.append(0)
    for i in range(len(list(contract_data_rows.keys()))):
        for j in contract_data_rows[keys[i]]:
            aux_row = copy(new_contract_datarow)
            for k in range(len(contract_data_rows[keys[i]][j])):
                if contract_title_rows[keys[i]][k] in end_title_row:
                    indice = end_title_row.index(contract_title_rows[keys[i]][k])
                    if contract_data_rows[keys[i]][j][k] is not None:
                        aux_row[indice] = copy(contract_data_rows[keys[i]][j][k])
            contract_data_rows[keys[i]][j] = copy(aux_row)

    data_rows = []
    title_row = convert_to_csv(end_title_row)
    for lista in contract_data_rows.keys():
        for j in contract_data_rows[lista].keys():
            data_rows.append(convert_to_csv(contract_data_rows[lista][j]))

    print(len(data_rows))
    if not os.path.isfile(outfile_dir) or os.stat(outfile_dir).st_size == 0:
        with open(outfile_dir, 'w') as outfile:
            outfile.write(title_row)
            for i in data_rows:
                outfile.write("\n")
                outfile.write(i)
    else:
        print('append')
        with open(outfile_dir, 'a+') as outfile:
            for i in data_rows:
                outfile.write("\n")
                outfile.write(i)

    print('name diccionary')

    with open(dic_dir, 'w') as out:
        out.write(json.dumps(diccionario.name_diccionario))


def convert_to_csv(input_data):
    my_list = input_data
    if type(input_data) is list:
        my_list = ','.join(map(str, input_data))
    return str(my_list)


def add_to_list(sub_title_row, sub_row, new_rows, id_contrato):
    global counter
    global title_row
    global data_rows
    global contract_data_rows
    global contract_title_rows
    counter = len(data_rows)
    aux_data_rows = {}
    original_copy = None
    if type(new_rows) is dict:
        if id_contrato not in contract_data_rows.keys():
            contract_data_rows[id_contrato] = {}
            contract_title_rows[id_contrato] = copy(title_row)
        for o in new_rows:
            contract_data_rows[id_contrato][o] = copy(new_rows[o])
    else:
        for o in data_rows:
            aux_data_rows[o] = copy(data_rows[o])
    if id_contrato:
        original_copy = deepcopy(contract_data_rows[id_contrato])
        counter = len(original_copy)
    actual_counter = copy(counter)
    for l in range(0, len(sub_title_row)):
        start_counter = 0
        start_counter = start_counter + actual_counter * l
        if start_counter > 0:
            for j in range(0, actual_counter):
                if id_contrato:
                    contract_data_rows[id_contrato][counter] = copy(original_copy[j])
                    counter = counter + 1
                else:
                    data_rows[counter] = copy(aux_data_rows[j])
                    counter = counter + 1
        for m in range(len(sub_title_row[l])):
            if id_contrato:
                if start_counter > 0:
                    for n in range(start_counter, counter):
                        appendNameValue(sub_row[l][m], sub_title_row[l][m], contract_title_rows[id_contrato],
                                        contract_data_rows[id_contrato][n], contract_data_rows)
                else:
                    for n in contract_data_rows[id_contrato]:
                        appendNameValue(sub_row[l][m], sub_title_row[l][m], contract_title_rows[id_contrato],
                                        contract_data_rows[id_contrato][n], contract_data_rows)
            else:
                for n in range(start_counter, counter):
                    appendNameValue(sub_row[l][m], sub_title_row[l][m], title_row, data_rows[n], data_rows)


def compare_rows(row1, title1, row2, title2):
    if len(title1) > len(title2):
        rellenar_campos_vacios(title1, row2, title2)
        rellenar_campos_vacios(title2, row1, title1)
    else:
        rellenar_campos_vacios(title2, row1, title1)
        rellenar_campos_vacios(title1, row2, title2)


def rellenar_campos_vacios(title1, row2, title2):
    for i in range(len(title1)):
        if title1[i] not in title2:
            if i < len(title2):
                title2.insert(i, title1[i])
                row2.insert(i, 0)
            else:
                title2.append(title1[i])
                row2.append(0)


def compare_rows_dict(title1, title2, rows1, rows2):
    for i in range(len(title2)):
        if title2[i] not in title1:
            if i < len(title1):
                title1.insert(i, title2[i])
                for k in rows1:
                    rows1[k].insert(i, 0)
            else:
                title1.append(title2[i])
                for k in rows1:
                    rows1[k].append(0)
    for i in range(len(title1)):
        if title1[i] not in title2:
            if i < len(title2):
                title2.insert(i, title1[i])
                for k in rows2:
                    rows2[k].insert(i, 0)
            else:
                title2.append(title1[i])
                for k in rows2:
                    rows2[k].append(0)


# electronicAuction
# written
convertToNumeric()
