from convert_data.getExchangeRate import getExchangeRate
from convert_data.convert_string import hashValue
from convert_data.convert_string import convertArray
from convert_data.convert_string import appendNameValue
from datetime import datetime
import convert_data.parties as parties


def budget_breakdown(budget, title_row, row):
    moneda_date = None
    start_date = None
    end_date = None
    max_extent_date = None
    budget_id = budget['id']
    appendNameValue(int(hashValue(budget['id'], False, True)), 'planning_budget_breakdown_id', title_row, row, None)
    if 'period' in budget:
        if 'startDate' in budget['period']:
            if budget['period']['startDate'] is not None:
                start_date = datetime.strptime(budget['period']['startDate'], '%Y-%m-%dT%H:%M:%SZ')
                moneda_date = start_date
                start_date = start_date.strftime('%s')
                appendNameValue(int(start_date), 'planning_budget_breakdown_startDate', title_row, row, None)
        if 'endDate' in budget['period']:
            if budget['period']['endDate'] is not None:
                end_date = datetime.strptime(budget['period']['endDate'], '%Y-%m-%dT%H:%M:%SZ')
                end_date = end_date.strftime('%s')
                appendNameValue(int(end_date), 'planning_budget_breakdown_endDate', title_row, row, None)
        if 'maxExtentDate' in budget['period']:
            if budget['period']['maxExtentDate'] is not None:
                max_extent_date = datetime.strptime(budget['period']['maxExtentDate'], '%Y-%m-%dT%H:%M:%SZ')
                max_extent_date = max_extent_date.strftime('%s')
                appendNameValue(int(max_extent_date), 'planning_budget_breakdown_maxExtentDate', title_row, row, None)
        if type(end_date) is str and type(start_date) is str:
            period = int(end_date) - int(start_date)
            appendNameValue(int(period), 'planning_budget_breakdown_period', title_row, row, None)
        if type(max_extent_date) is str and type(start_date) is str:
            period = int(max_extent_date) - int(start_date)
            appendNameValue(int(period), 'planning_budget_breakdown_maxExtentPeriod', title_row, row, None)

    moneda = budget["amount"]["currency"]
    amount = budget["amount"]["amount"]
    if moneda is not None and amount is not None:
        exchange_rate = getExchangeRate(moneda, moneda_date)
        appendNameValue(amount * exchange_rate, 'planning_budget_breakdown_amount', title_row, row, None)
        appendNameValue(convertArray(moneda, 'currency'), 'planning_budget_breakdown_currency', title_row, row, None)
    moneda = budget["commitedAmount"]["currency"]
    amount = budget["commitedAmount"]["amount"]
    if moneda is not None and amount is not None:
        exchange_rate = getExchangeRate(moneda, moneda_date)
        appendNameValue(amount * exchange_rate, 'planning_budget_breakdown_commitedAmount', title_row, row, None)
        appendNameValue(convertArray(moneda, 'currency'), 'planning_budget_breakdown_commitedAmount_currency', title_row, row, None)
    if 'status' in budget:
        budget_breakdown_status = convertArray(budget['status'], 'budget_breakdown_status')
        appendNameValue(budget_breakdown_status, 'planning_budget_breakdown_status', title_row, row, None)
    if 'availabilityCertificateNumber' in budget:
        certificate = budget["availabilityCertificateNumber"]
        appendNameValue(certificate, 'planning_budget_breakdown_availabilityCertificateNumber', title_row, row, None)
    if 'sourceParty' in budget:
        party = parties.parties_list[budget['sourceParty']['id']]
        for i in party['title_row']:
            title_row.append('planning_budget_breakdown_sourceParty_' + i)
        for i in party['row']:
            row.append(i)
    # return [budget_id, title_row, row]
    return [moneda_date]
