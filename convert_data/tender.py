from convert_data.getExchangeRate import getExchangeRate
from convert_data.convert_string import convertArray
import convert_data.parties as parties
from datetime import datetime
from convert_data.items import items
from convert_data.documents import documents
from convert_data.convert_string import appendNameValue
from convert_data.convert_string import hashValue


def tender(tender_data, title_row, row):
    tender_id = tender_data['id']
    moneda = tender_data["value"]["currency"]
    amount = tender_data["value"]["amount"]
    end_date = None
    start_date = None
    max_extent_date = None
    moneda_date = None
    mon_date = None
    tender_end_date = None
    submissionMethod = None
    if tender_id:
        appendNameValue(tender_id, 'tender_id', title_row, row, None)
    if 'tenderPeriod' in tender_data:
        if 'startDate' in tender_data['tenderPeriod']:
            if tender_data['tenderPeriod']['startDate'] is not None:
                start_date = datetime.strptime(tender_data['tenderPeriod']['startDate'], '%Y-%m-%dT%H:%M:%SZ')
                month = start_date.month
                moneda_date = start_date
                mon_date = start_date
                start_date = start_date.strftime('%s')
                appendNameValue(int(month), 'tenderPeriod_startDateMonth', title_row, row, None)
        if 'endDate' in tender_data['tenderPeriod']:
            if tender_data['tenderPeriod']['endDate'] is not None:
                end_date = datetime.strptime(tender_data['tenderPeriod']['endDate'], '%Y-%m-%dT%H:%M:%SZ')
                month = end_date.month
                tender_end_date = end_date.strftime('%s')
                appendNameValue(int(month), 'tenderPeriod_endDateMonth', title_row, row, None)
        if 'maxExtentDate' in tender_data['tenderPeriod']:
            if tender_data['tenderPeriod']['maxExtentDate'] is not None:
                max_extent_date = datetime.strptime(tender_data['tenderPeriod']['maxExtentDate'], '%Y-%m-%dT%H:%M:%SZ')
                month = max_extent_date.month
                max_extent_date = max_extent_date.strftime('%s')
                appendNameValue(int(month), 'tenderPeriod_maxExtentDateMonth', title_row, row, None)
        if type(tender_end_date) is str and type(start_date) is str:
            period = int(tender_end_date) - int(start_date)
            appendNameValue(int(period), 'tenderPeriod_period', title_row, row, None)
        if type(max_extent_date) is str and type(start_date) is str:
            period = int(max_extent_date) - int(start_date)
            appendNameValue(int(period), 'tenderPeriod_maxExtentPeriod', title_row, row, None)
    if 'enquiryPeriod' in tender_data:
        end_date = None
        start_date = None
        if 'startDate' in tender_data['enquiryPeriod']:
            if tender_data['enquiryPeriod']['startDate'] is not None:
                start_date = datetime.strptime(tender_data['enquiryPeriod']['startDate'], '%Y-%m-%dT%H:%M:%SZ')
                month = start_date.month
                if moneda_date is None:
                    moneda_date = start_date
                mon_date = start_date
                start_date = start_date.strftime('%s')
                appendNameValue(int(month), 'tender_enquiryPeriod_startDateMonth', title_row, row, None)
        if 'endDate' in tender_data['enquiryPeriod']:
            if tender_data['enquiryPeriod']['endDate'] is not None:
                end_date = datetime.strptime(tender_data['enquiryPeriod']['endDate'], '%Y-%m-%dT%H:%M:%SZ')
                month = end_date.month
                if moneda_date is None:
                    moneda_date = end_date
                if mon_date is None:
                    mon_date = end_date
                end_date = end_date.strftime('%s')
                appendNameValue(int(month), 'tender_enquiryPeriod_endDateMonth', title_row, row, None)
        if type(end_date) is str and type(start_date) is str:
            period = int(end_date) - int(start_date)
            appendNameValue(int(period), 'tender_enquiryPeriod_period', title_row, row, None)
    if 'awardPeriod' in tender_data:
        end_date = None
        start_date = None
        if 'startDate' in tender_data['awardPeriod']:
            if tender_data['awardPeriod']['startDate'] is not None:
                start_date = datetime.strptime(tender_data['awardPeriod']['startDate'], '%Y-%m-%dT%H:%M:%SZ')
                month = start_date.month
                start_date = start_date.strftime('%s')
                appendNameValue(int(month), 'tender_awardPeriod_startDateMonth', title_row, row, None)
        if 'endDate' in tender_data['awardPeriod']:
            if tender_data['awardPeriod']['endDate'] is not None:
                end_date = datetime.strptime(tender_data['awardPeriod']['endDate'], '%Y-%m-%dT%H:%M:%SZ')
                month = end_date.month
                end_date = end_date.strftime('%s')
                appendNameValue(int(month), 'tender_awardPeriod_endDateMonth', title_row, row, None)
        if type(end_date) is str and type(start_date) is str:
            period = int(end_date) - int(start_date)
            appendNameValue(int(period), 'tender_awardPeriod_period', title_row, row, None)
    if moneda is not None and amount is not None:
        exchange_rate = getExchangeRate(moneda, moneda_date)
        planning_budget_amount = amount * exchange_rate
        appendNameValue(planning_budget_amount, 'tender_amount', title_row, row, None)
        # planning_budget_currency = convertArray(moneda, 'currency')
        # appendNameValue(planning_budget_currency, 'tender_currency', title_row, row, None)
    if 'status' in tender_data:
        award_status = convertArray(tender_data['status'], 'status')
        appendNameValue(award_status, 'tender_status', title_row, row, None)
    if 'hasEnquiries' in tender_data:
        if tender_data['hasEnquiries'] == "false":
            tender_hasenquiries = 0
        else:
            tender_hasenquiries = 1
        appendNameValue(tender_hasenquiries, 'tender_hasEnquiries', title_row, row, None)
    if 'procuringEntity' in tender_data:
        party = parties.parties_list[tender_data['procuringEntity']['id']]
        for i in party['title_row']:
            title_row.append('tender_procuringEntity_' + i)
        for i in party['row']:
            row.append(i)
    if 'procurementMethodDetails' in tender_data:
        award_status = convertArray(tender_data['procurementMethodDetails'], 'procurementMethodDetails')
        appendNameValue(award_status, 'tender_procurementMethodDetails', title_row, row, None)
    if 'numberOfTenderers' in tender_data:
        appendNameValue(tender_data['numberOfTenderers'], 'tender_numberOfTenderers', title_row, row, None)
    aux_row = []
    if 'documents' in tender_data:
        for document in tender_data['documents']:
            if 'documentType' in document:
                aux_row.append(document['documentType'])
        document_type = convertArray(aux_row, 'tenderDocumentType')
        appendNameValue(document_type, 'tender_documentTypes', title_row, row, None)
    return [tender_id, title_row, row, mon_date, tender_end_date]
