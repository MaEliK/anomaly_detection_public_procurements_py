from copy import deepcopy

from sklearn import preprocessing
from sklearn.ensemble import IsolationForest
import pandas as pd
import os
import json
import time
from joblib import dump, load
start = time. time()
# Pre-processing data
name = 'tender_written'
version = ''
textfile = "../convert_data/convertedData/converted_" + name + "_training_set" + version + ".csv"
forest_size = [20, 50, 80, 100, 200]
sample_size = [20, 40, 60, 80, 100]
if 'adendas' in textfile:
    max_features = [19]
elif 'tender' in textfile:
    max_features = [23]
else:
    max_features = [40]

repeats = ['1']

name = name + '' + version

input_contract_data = pd.read_csv(textfile, sep=',')

if 'tender' in textfile:
    contract_data = input_contract_data.drop(['id', 'ocid', 'tender_id'], axis=1)
elif 'adenda' in textfile:
    contract_data = input_contract_data.drop(['id', 'ocid', 'contract_id', 'adenda_id'], axis=1)
    contract_data = contract_data.drop(['contract_supplier_roles', 'contract_status'], axis=1)
    print(len(contract_data.columns))
else:
    contract_data = input_contract_data.drop(['id', 'ocid', 'tender_id', 'award_id', 'contract_id'], axis=1)
    contract_data = contract_data.drop(['contract_supplier_roles', 'tender_status', 'award_status', 'contract_status'], axis=1)
print(len(input_contract_data.columns))
contract_data.replace(to_replace='None', value=0, inplace=True)
contract_data.replace(to_replace='NaN', value=0, inplace=True)
contract_data.fillna(0, inplace=True)

for estimator in forest_size:
    for sample in sample_size:
        if sample <= len(contract_data) + 10:
            for feature in max_features:
                for rep in repeats:
                    # fit the model
                    clf = IsolationForest(n_estimators=estimator, max_samples=sample, behaviour='new',
                                          random_state=None, n_jobs=-1, max_features=feature, contamination='auto')
                    clf.fit(contract_data, y=None, sample_weight=None)
                    prediction = clf.predict(contract_data)
                    dump(clf, 'models/' + name + '/' + name +
                              '_model_estimators_' + str(estimator) + '_samples_' +
                         str(sample) + '_features_' + str(feature) + '.joblib')

                    # 1 is inlier, -1 outlier
                    scores = clf.decision_function(contract_data)

                    # print(contract_data["contract_id"])
                    predictions = pd.DataFrame()
                    predictions["predicted_class"] = prediction
                    predictions["scores"] = scores
                    predictions["id"] = input_contract_data["id"]
                    if 'tender' in textfile:
                        predictions["tender_id"] = input_contract_data["tender_id"]
                        headers = ['Predicted Class', 'Scores', 'OCDS ID', 'Tender Id']
                    elif 'contra' in textfile or 'adenda' in textfile:
                        predictions["contract_id"] = input_contract_data["contract_id"]
                        headers = ['Predicted Class', 'Scores', 'OCDS ID', 'Tender Id', 'Contract Id']
                    if 'adenda' in textfile:
                        predictions["adenda_id"] = input_contract_data["adenda_id"]
                        headers = ['Predicted Class', 'Scores', 'OCDS ID', 'Contract Id', 'Adenda Id']
                    end = time. time()
                    ex_time = end - start
                    print('trees: ' + str(estimator) + ' samples: ' + str(sample) + ' attributes: ' + str(feature) + ' time: ' + str(ex_time))

                    # predictions.to_csv('iforest_results/' + name + '/'
                    #                    'iforest_' + name +
                    #                    '_estimators_' + str(estimator) + '_samples_' + str(sample) + '_features_' + str(feature) + '.csv',
                    #                    header=headers, index=None, sep=',', mode='w')
print(name)
