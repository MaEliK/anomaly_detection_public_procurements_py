import os
import pandas as pd
import re


# directory = 'denuncias_analisis/new_data_tender/'
directory = 'iforest_results/tender_electronicAuction/'
set_direccion = 'validaciones/'

# new_data18 = 'convertedData/converted_contratos_2018_fill_empty.csv'
# new_contract_data18 = pd.read_csv(new_data18)
# new_contract_data18 = new_contract_data18.fillna(0)
# new_data19 = 'convertedData/converted_contratos_2019_fill_empty.csv'
# new_contract_data19 = pd.read_csv(new_data19)
# new_contract_data19 = new_contract_data19.fillna(0)
# new_contract_data = pd.concat([new_contract_data18, new_contract_data19], ignore_index=True)
#
# for filename in os.listdir(directory):
#     print(filename)
#     anomaly_score = pd.read_csv(directory + filename)
#     data_id = []
#     data_score = []
#     for (index, value) in anomaly_score["OCDS ID"].iteritems():
#         if value in new_contract_data.values:
#             data_id.append(value)
#             data_score.append(anomaly_score["Scores"][index])
#     anomaly_score_df = pd.DataFrame()
#     anomaly_score_df['id'] = data_id
#     anomaly_score_df['Scores'] = data_score
#     print("length tender trained: " + str(len(anomaly_score_df)))
#     print("anomalies: " + str(len(anomaly_score_df[anomaly_score_df["Scores"] < 0])))

resultados = pd.DataFrame()
result_estimators = []
result_samples = []
result_features = []
result_anomalias = []
result_anomalias_muy_anomalos = []
result_anomalias_muy_normales = []
total = []
for filename in sorted(os.listdir(directory)):
    # filename = 'iforest_tender_written_estimators_5_samples_5_features_1.csv'
    # print(filename)

    estimator = re.search('estimators_(.*)_samples', filename)
    result_estimators.append(int(estimator.group(1)))
    samples = re.search('samples_(.*)_features', filename)
    result_samples.append(int(samples.group(1)))
    features = re.search('features_(.*).csv', filename)
    result_features.append(int(features.group(1)))

    anomaly_score = pd.read_csv(directory + filename)
    total.append(len(anomaly_score))
    result_anomalias.append(len(anomaly_score[anomaly_score["Scores"] < 0]))
    result_anomalias_muy_anomalos.append(len(anomaly_score[anomaly_score["Scores"] < -0.1]))
    result_anomalias_muy_normales.append(len(anomaly_score[anomaly_score["Scores"] > 0.1]))

resultados["estimators"] = result_estimators
resultados["samples"] = result_samples
resultados["features"] = result_features
resultados["anomalias"] = result_anomalias
resultados["muy_anormales"] = result_anomalias_muy_anomalos
resultados["muy_normales"] = result_anomalias_muy_normales
resultados["total"] = total
# print(resultados)
resultados.sort_values(by=['estimators', 'samples', 'features'], inplace=True)

print(resultados)
resultados.to_csv(
    set_direccion + 'tender_electronicAuction_general.csv', header=['estimators', 'samples', 'features', 'anomalias', 'muy_anormales',
                                                      'muy_normales', 'total'], index=None, sep=',', mode='w')