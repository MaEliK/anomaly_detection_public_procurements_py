import pandas as pd
import os
import json
import numpy as np
import datetime

from validaciones.validate_model_with_new_data import validate_with_new_data

name = 'contratos_written'
folder_name = ''
version = ''

# contratos_written
# file_name = name + folder_name + version + '_model_estimators_50_samples_20_features_40_1'
file_name = name + folder_name + version + '_model_estimators_100_samples_20'

# contratos_electronicAuction
# file_name = name + folder_name + version + '_model_estimators_100_samples_20_features_40_5'

# adendas_electronicAuction
# file_name = name + folder_name + version + '_model_estimators_20_samples_40_features_19_3'
#
# adendas_written
# file_name = name + folder_name + version + '_model_estimators_80_samples_20_features_19_1'
# file_name = name + folder_name + version + '_model_estimators_200_samples_40_features_19_1'

# tender_electronicAuction
# file_name = name + folder_name + version + '_model_estimators_80_samples_20_features_23_3'

# tender_written
# file_name = name + folder_name + version + '_model_estimators_50_samples_20_features_23_3'

original_data_file = "../convert_data/convertedData_1/converted_" + name + "_original_data.csv"

textfile_training = "../convert_data/convertedData/converted_" + name + ".csv"
score_file = 'models/' + name + folder_name + version + '/' + file_name + '.joblib'

scores = validate_with_new_data(score_file, textfile_training, ';')
original_data = pd.read_csv(original_data_file, sep=";", error_bad_lines=False)

# if results in file
# scores_file = 'iforest_results/contratos_written_training_smart_selection_with_anomalies_validation_model_estimators_100_samples_50_features_44_4.csv'
# scores = pd.read_csv(scores_file)
# print(scores)

proceso_id = ''

original_data.replace(to_replace='None', value=0, inplace=True)
original_data.replace(to_replace='NaN', value=0, inplace=True)
original_data.fillna(0, inplace=True)

resultados = original_data.copy(deep=True)
if 'tender' in name:
    proceso_id = 'tender_id'
    original_data = original_data.drop(['tender_numberOfTenderers'], axis=1)
elif 'contrato' in name:
    print(name)
    proceso_id = 'contract_id'
    print(proceso_id)
    original_data = original_data.drop(['contract_supplier_roles', 'tender_status', 'award_status', 'contract_status'],
                                       axis=1)
    print(original_data.columns)
    # scores.rename(columns={'Contract Id': 'contract_id'}, inplace=True)
    # scores.rename(columns={'Scores': 'scores'}, inplace=True)
    original_data["tenderPeriod_period"] = original_data["tenderPeriod_period"] / 86400
    original_data["tenderPeriod_maxExtentPeriod"] = original_data["tenderPeriod_maxExtentPeriod"] / 86400
    original_data["tender_enquiryPeriod_period"] = original_data["tender_enquiryPeriod_period"] / 86400
    original_data["tender_awardPeriod_period"] = original_data["tender_awardPeriod_period"] / 86400
    original_data["period_tender_award"] = original_data["period_tender_award"] / 86400
    original_data["period_tender_award"] = original_data["period_tender_award"].astype('str').replace('-', '').astype('float')
    original_data["contract_period"] = original_data["contract_period"] / 86400
    original_data["contract_period"] = original_data["contract_period"]
    original_data["contract_dateSigned_period"] = original_data["contract_dateSigned_period"] / 86400
    original_data["award_contractStartDate_period"] = (original_data["award_contractStartDate_period"] / 86400)
    original_data["award_contractStartDate_period"] = original_data["award_contractStartDate_period"].abs()
    original_data["award_contractSignedDate_period"] = (original_data["award_contractSignedDate_period"] / 86400)
    print(original_data["award_contractSignedDate_period"])
    original_data["award_contractSignedDate_period"] = original_data["award_contractSignedDate_period"].abs()
    print(original_data["award_contractSignedDate_period"])
    original_data["contract_dateSigned"] = pd.to_datetime(original_data["contract_dateSigned"], unit='s')
    original_data["contract_dateSigned"] = pd.DatetimeIndex(original_data["contract_dateSigned"]).month
    original_data.rename(columns={'contract_dateSigned': 'contract_dateSignedMonth'}, inplace=True)
    resultados = original_data.copy(deep=True)
elif 'adenda' in name:
    proceso_id = 'adenda_id'
    original_data = original_data.drop(['contract_supplier_roles', 'contract_status'], axis=1)
    scores.rename(columns={'adenda_contract_id': 'adenda_id'}, inplace=True)
    original_data.rename(columns={'contract_dateSigned': 'contract_dateSignedMonth'}, inplace=True)
    resultados.rename(columns={'contract_dateSigned': 'contract_dateSignedMonth'}, inplace=True)
    resultados['adenda_amount'] = ""
print(original_data)
print(len(original_data))
print(original_data.columns)
print(len(original_data.columns))

resultados["scores"] = ""
if 'tender' in name:
    resultados = resultados.drop(['tender_numberOfTenderers'], axis=1)
original_data.fillna(0)
print(original_data.columns)
print(scores.columns)
for (index, value) in scores[proceso_id].iteritems():
    pos = np.where(resultados[proceso_id] == value)
    if 'adenda' in name:
        resultados['adenda_amount'][pos[0]] = scores.loc[index, 'adenda_amount']
        resultados["contract_period"][pos[0]] = resultados["contract_period"][pos[0]].astype(float) / 86400
        resultados["award_contractStartDate_period"][pos[0]] = (
                    resultados["award_contractStartDate_period"][pos[0]].astype(float) / 86400)
        resultados["contract_dateSigned_period"][pos[0]] = resultados["contract_dateSigned_period"][pos[0]].astype(
            float) / 86400
        resultados["award_contractSignedDate_period"][pos[0]] = (
                    resultados["award_contractSignedDate_period"][pos[0]].astype(float) / 86400)
        resultados["award_contractSignedDate_period"][pos[0]] = resultados["award_contractSignedDate_period"][pos[0]].astype(
            float).abs()
        resultados["adenda_contract_dateSigned_period"][pos[0]] = resultados[
                                                                 "adenda_contract_dateSigned_period"][pos[0]].astype(
            float) / 86400
        print(resultados.columns)
        resultados["contract_dateSignedMonth"][pos[0]] = pd.to_datetime(resultados["contract_dateSignedMonth"][pos[0]], unit='s')
        resultados["contract_dateSignedMonth"][pos[0]] = pd.DatetimeIndex(resultados["contract_dateSignedMonth"][pos[0]]).month

    resultados['scores'][pos[0]] = scores.loc[index, 'scores']
print(resultados)
# resultados.sort_values('scores', inplace=True, ascending=True, axis=0)

print(resultados.columns)

resultados.to_csv('validation_decision_tree/' + file_name + '.csv',
                  header=resultados.columns, index=None, sep=';', mode='w')
